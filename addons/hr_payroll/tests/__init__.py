# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import test_payslip_flow
from . import test_benefit
from . import test_multi_contract
from . import test_calendar_sync
from . import test_leave
