# -*- coding:utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import l10n_be_hr_payroll
from . import res_config_settings
from . import res_users
